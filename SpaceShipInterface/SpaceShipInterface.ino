

int switchState1 = 0;
int switchState2 = 0;
void setup() 
{
 pinMode(5, OUTPUT);
 pinMode(4, OUTPUT);
 pinMode(3, OUTPUT);
 pinMode(6, OUTPUT);
 pinMode(2, INPUT);
 pinMode(7, INPUT);
}

void loop() 
{
switchState1 = digitalRead(2);
switchState2 = digitalRead(7);
// if the switch is off turn on the green light
if (switchState1 == LOW)
{
    digitalWrite(3, HIGH);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
  }
// the switch is on, flash red LEDs
else
 {
  digitalWrite(3, LOW);
  digitalWrite(4, LOW);
  digitalWrite(5, HIGH);
  digitalWrite(6, LOW);
  delay(250);
  digitalWrite(4, HIGH);
  digitalWrite(5, LOW);
  delay(250);
  }
  if (switchState2 == LOW)
  {
    digitalWrite(3, HIGH);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
  }
  else{
    digitalWrite(3, LOW);
    digitalWrite(6, HIGH);
  }
}
